from flask import Flask, render_template
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)


@app.route('/')
def channels_list():
    return render_template('channels_list.html')


@app.route('/channel/<id>')
def channel(id):
    return render_template('draw.html', connection_id=id)


app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run(host='0.0.0.0')
