const express = require('express')
const path = require('path')

const PORT = process.env.PORT || 3000;

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const viewsDir = path.join(__dirname, 'views')
app.use(express.static(viewsDir))
app.use(express.static(path.join(__dirname, './public')))
app.use(express.static(path.join(__dirname, './weights')))

app.get('/', (req, res) => res.sendFile(path.join(viewsDir, 'webcamFaceLandmarkDetection.html')))

app.listen(PORT, () => console.log(`Listening on port ${PORT}!`))