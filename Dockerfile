FROM python:3.6

RUN apt-get -y update
RUN apt-get install -y --fix-missing \
    build-essential \
    cmake \
    git \
    wget \
    curl \
    pkg-config \
    python3-dev \
    python3-numpy \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install nodejs
ADD requiremets.txt /requirements.txt
COPY face_api/package*.json /
RUN pip install -r requirements.txt

ADD . /

#RUN cd face_api/
RUN npm i
RUN npm install -g forever
RUN chmod +x /sockets.sh

EXPOSE 8844
EXPOSE 8000
EXPOSE 3000

ENTRYPOINT forever start face_api/server.js && ./sockets.sh && gunicorn -w 2 -b :8000 app:app