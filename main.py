# Note this is targeted at python 3
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.options
import json
import config

WRITE_TYPE = 'write'
SUBSCRIBE_TYPE = 'subscribe'
POST_TYPE = 'post'


class ChannelHandler(tornado.websocket.WebSocketHandler):
    """
    Handler that handles a websocket channel
    """
    write_connections = dict()
    subscribe_connections = dict()
    info_connections = list()

    @classmethod
    def urls(cls):
        return [
            (r'/', cls, {}),  # Route/Handler/kwargs
        ]

    def open(self):
        """
        Client opens a websocket
        """

    def on_message(self, message):
        """
        Message received on channel
        """
        face = {
            'contour': [],
            'right_eye': [],
            'left_eye': [],
            'right_eyebrow': [],
            'left_eyebrow': [],
            'lips': [],
            'nose': []
        }
        inverted_face = {
            'contour': [],
            'right_eye': [],
            'left_eye': [],
            'right_eyebrow': [],
            'left_eyebrow': [],
            'lips': [],
            'nose': []
        }
        try:
            message = json.loads(message)
        except ValueError:
            print('Invalid JSON.')
            return

        if not {'type'}.issubset(message):
            print('"type" is required')
            return

        if message['type'] == POST_TYPE:
            if not {'data'}.issubset(message):
                print('DATA is required for posting')
                return
            index = list(self.write_connections.keys())[list(self.write_connections.values()).index(self)]

            height = message['data']['image']['height']
            # Sorting our data by face parts
            for point in message['data']['points']:
                inverted_point = {
                    '_x': point['_x'],
                    '_y': height - point['_y']  # inverting y
                }
                point_index = message['data']['points'].index(point)
                if point_index in range(0, 17):
                    face['contour'].append(point)
                    inverted_face['contour'].append(inverted_point)
                elif point_index in range(17, 22):
                    face['left_eyebrow'].append(point)
                    inverted_face['left_eyebrow'].append(inverted_point)
                elif point_index in range(22, 27):
                    face['right_eyebrow'].append(point)
                    inverted_face['right_eyebrow'].append(inverted_point)
                elif point_index in range(27, 36):
                    face['nose'].append(point)
                    inverted_face['nose'].append(inverted_point)
                elif point_index in range(36, 42):
                    face['left_eye'].append(point)
                    inverted_face['left_eye'].append(inverted_point)
                elif point_index in range(42, 48):
                    face['right_eye'].append(point)
                    inverted_face['right_eye'].append(inverted_point)
                else:
                    face['lips'].append(point)
                    inverted_face['lips'].append(inverted_point)

            # Sending sorted data to the subscribers of this channel
            if self.subscribe_connections and index in self.subscribe_connections.keys():
                [con.write_message(
                    str({'face': face, 'inverted_face': inverted_face}).replace('\\', '').replace('\'', '"')) for
                    con in self.subscribe_connections[index]]

        elif message['type'] == WRITE_TYPE:
            self.write_connections[len(self.write_connections)] = self
            self.push_info()

        elif message['type'] == SUBSCRIBE_TYPE:
            if not {'id'}.issubset(message):
                print('ID is required for subscribe')
                return
            if message['id'] in self.subscribe_connections.keys():
                self.subscribe_connections[message['id']].append(self)
            else:
                self.subscribe_connections[message['id']] = [self]
        else:
            self.info_connections.append(self)
            self.write_message(str(len(self.write_connections)))

    def on_close(self):
        """
        Channel is closed
        """
        for (w_key, writer) in self.write_connections.items():
            if self == writer:
                del self.write_connections[w_key]
                self.push_info()
                break
        if self in self.info_connections:
            self.info_connections.remove(self)
        for (s_key, subscriber_list) in self.subscribe_connections.items():
            if self in subscriber_list:
                if len(subscriber_list) == 1:
                    del self.subscribe_connections[s_key]
                    break
                else:
                    subscriber_list.remove(self)
        print('close channel')

    def check_origin(self, origin):
        """
        Override the origin check if needed
        """
        return True

    def push_info(self):
        """
            Send info about available writer channels to all info_connections
        """
        print(list(self.write_connections.keys()))
        [con.write_message(str(list(self.write_connections.keys()))) for con in
         self.info_connections]


def main():
    # Create tornado application and supply URL routes
    app = tornado.web.Application(ChannelHandler.urls())

    # Setup HTTP Server
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(config.SOCKET_PORT, config.HOST)

    # Start IO/Event loop
    try:
        loop = tornado.ioloop.IOLoop.current()
        loop.start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.current().stop()


if __name__ == '__main__':
    main()
