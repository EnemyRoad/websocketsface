WebSockets service for transporting face point coordinates.

* build composer container by command `docker-composer build`
* run container by command `docker-composer up`
* check in browser how it works. Recognizing app running at port `8006` and draw app at port `8005`